CREATE SEQUENCE checkpoint_id_seq;
CREATE SEQUENCE chip_id_seq;
CREATE SEQUENCE competitor_id_seq;
CREATE SEQUENCE event_id_seq;
CREATE SEQUENCE person_id_seq;
CREATE SEQUENCE result_id_seq;

CREATE TABLE checkpoint (
  id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('checkpoint_id_seq')
);

CREATE TABLE chip (
  id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('chip_id_seq')
);

CREATE TABLE person (
  id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('person_id_seq'),
  name TEXT NOT NULL
);

CREATE TABLE event (
  id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('event_id_seq'),
  name TEXT NOT NULL,
  start BIGINT NOT NULL,
  finish BIGINT NOT NULL
);

CREATE TABLE competitor (
  id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('competitor_id_seq'),
  person BIGINT NOT NULL,
  event BIGINT NOT NULL,
  chip BIGINT NOT NULL,
  number BIGINT NOT NULL
);

CREATE TABLE result (
  id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('result_id_seq'),
  time TIMESTAMP NOT NULL,
  competitor BIGINT NOT NULL,
  checkpoint BIGINT NOT NULL,
  event BIGINT NOT NULL
);

ALTER TABLE ONLY event ADD CONSTRAINT fk__event__start_checkpoint FOREIGN KEY (start) REFERENCES checkpoint (id);
ALTER TABLE ONLY event ADD CONSTRAINT fk__event__finish_checkpoint FOREIGN KEY (finish) REFERENCES checkpoint (id);

ALTER TABLE ONLY competitor ADD CONSTRAINT fk__competitor__person FOREIGN KEY (person) REFERENCES person (id);
ALTER TABLE ONLY competitor ADD CONSTRAINT fk__competitor__event FOREIGN KEY (event) REFERENCES event (id);
ALTER TABLE ONLY competitor ADD CONSTRAINT fk__competitor__chip FOREIGN KEY (chip) REFERENCES chip (id);

ALTER TABLE ONLY result ADD CONSTRAINT fk__result__competitor FOREIGN KEY (competitor) REFERENCES competitor (id);
ALTER TABLE ONLY result ADD CONSTRAINT fk__result__checkpoint FOREIGN KEY (checkpoint) REFERENCES checkpoint (id);
ALTER TABLE ONLY result ADD CONSTRAINT fk__result__event FOREIGN KEY (event) REFERENCES event (id);
