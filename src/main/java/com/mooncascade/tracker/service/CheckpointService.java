package com.mooncascade.tracker.service;

import java.util.List;

import com.mooncascade.tracker.domain.Checkpoint;
import com.mooncascade.tracker.repository.CheckpointRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CheckpointService {

    private CheckpointRepository checkpointRepository;

    @Autowired
    public CheckpointService(CheckpointRepository checkpointRepository) {
        this.checkpointRepository = checkpointRepository;
    }

    public Checkpoint create() {
        return checkpointRepository.save(new Checkpoint());
    }

    public List<Checkpoint> list() {
        return checkpointRepository.findAll();
    }
}
