package com.mooncascade.tracker.service;

import com.mooncascade.tracker.domain.*;
import com.mooncascade.tracker.repository.ResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ResultService {

    private ResultRepository resultRepository;
    private EventService eventService;
    private ChipService chipService;
    private CompetitorService competitorService;

    @Autowired
    public ResultService(ResultRepository resultRepository,
                         EventService eventService,
                         ChipService chipService,
                         CompetitorService competitorService) {
        this.resultRepository = resultRepository;
        this.eventService = eventService;
        this.chipService = chipService;
        this.competitorService = competitorService;
    }

    public List<Result> start(Long eventId, Long chipId, Date time) {
        Event event = eventService.get(eventId);
        Chip chip = chipService.get(chipId);
        return registerChipAndReturnResults(event, event.getStart(), chip, time);
    }

    public List<Result> finish(Long eventId, Long chipId, Date time) {
        Event event = eventService.get(eventId);
        Chip chip = chipService.get(chipId);
        return registerChipAndReturnResults(event, event.getFinish(), chip, time);
    }

    private List<Result> registerChipAndReturnResults(Event event, Checkpoint checkpoint, Chip chip, Date time) {
        Competitor competitor = competitorService.getBy(event, chip);
        buildAndSave(event, checkpoint, competitor, time);
        return resultRepository.findBy(event.getId(), checkpoint.getId());
    }

    private void buildAndSave(Event event, Checkpoint checkpoint, Competitor competitor, Date time) {
        Result result = new Result();
        result.setCheckpoint(checkpoint);
        result.setCompetitor(competitor);
        result.setEvent(event);
        result.setTime(time);
        resultRepository.save(result);
    }
}
