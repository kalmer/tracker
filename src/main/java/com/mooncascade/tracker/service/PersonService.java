package com.mooncascade.tracker.service;

import com.mooncascade.tracker.domain.Person;
import com.mooncascade.tracker.repository.PersonRepository;
import org.springframework.stereotype.Service;

@Service
public class PersonService {

    private PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public Person save(Person person) {
        return personRepository.save(person);
    }
}
