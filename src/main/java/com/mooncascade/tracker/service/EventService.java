package com.mooncascade.tracker.service;

import com.mooncascade.tracker.domain.Event;
import com.mooncascade.tracker.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EventService {

    private EventRepository eventRepository;

    @Autowired
    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public List<Event> list() {
        return eventRepository.findAll();
    }

    public Event get(Long eventId) {
        return eventRepository.getOne(eventId);
    }

    public Event create(Event event) {
        return eventRepository.save(event);
    }
}
