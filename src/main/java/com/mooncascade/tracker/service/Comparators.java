package com.mooncascade.tracker.service;

import com.mooncascade.tracker.information.ResultInformation;

import java.util.Comparator;

public class Comparators {

    public static Comparator<ResultInformation> RESULT_COMPARATOR = Comparator.comparing(ResultInformation::getTime);

}
