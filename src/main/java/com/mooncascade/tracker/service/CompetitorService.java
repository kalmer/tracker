package com.mooncascade.tracker.service;

import com.mooncascade.tracker.domain.Chip;
import com.mooncascade.tracker.domain.Competitor;
import com.mooncascade.tracker.domain.Event;
import com.mooncascade.tracker.repository.CompetitorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class CompetitorService {

    private CompetitorRepository competitorRepository;

    @Autowired
    public CompetitorService(CompetitorRepository competitorRepository) {
        this.competitorRepository = competitorRepository;
    }

    public Competitor create(Competitor competitor) {
        return competitorRepository.save(competitor);
    }

    public Competitor getBy(Event event, Chip chip) {
        List<Competitor> competitors = competitorRepository.registeredToEvent(event.getId(), chip.getId());
        return competitors.get(0);
    }
}
