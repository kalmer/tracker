package com.mooncascade.tracker.service;

import java.util.List;

import com.mooncascade.tracker.domain.Chip;
import com.mooncascade.tracker.repository.ChipRepository;
import org.springframework.stereotype.Service;

@Service
public class ChipService {

    private ChipRepository chipRepository;

    public ChipService(ChipRepository chipRepository) {
        this.chipRepository = chipRepository;
    }

    public Chip get(Long chipId) {
        return chipRepository.getOne(chipId);
    }

    public List<Chip> list() {
        return chipRepository.findAll();
    }

    public Chip create() {
        return chipRepository.save(new Chip());
    }
}
