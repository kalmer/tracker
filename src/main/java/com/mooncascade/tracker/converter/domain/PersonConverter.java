package com.mooncascade.tracker.converter.domain;

import com.mooncascade.tracker.converter.AbstractConverter;
import com.mooncascade.tracker.converter.Converter;
import com.mooncascade.tracker.converter.DomainConverter;
import com.mooncascade.tracker.domain.Person;
import com.mooncascade.tracker.information.PersonInformation;
import com.mooncascade.tracker.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PersonConverter extends AbstractConverter<PersonInformation, Person>
        implements Converter<PersonInformation, Person>, DomainConverter<Person> {

    private PersonRepository personRepository;

    @Autowired
    public PersonConverter(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    protected Person map(PersonInformation personInformation) {
        Person person = get(personInformation.getId());
        person.setName(personInformation.getName());
        return person;
    }

    @Override
    public Person get(Long id) {
        if (id != null) {
            Optional<Person> person = personRepository.findById(id);
            if (person.isPresent()) {
                return person.get();
            }
        }
        return new Person();
    }
}
