package com.mooncascade.tracker.converter.domain;

import com.mooncascade.tracker.converter.AbstractConverter;
import com.mooncascade.tracker.converter.Converter;
import com.mooncascade.tracker.converter.DomainConverter;
import com.mooncascade.tracker.domain.Event;
import com.mooncascade.tracker.information.EventInformation;
import com.mooncascade.tracker.repository.CheckpointRepository;
import com.mooncascade.tracker.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EventConverter extends AbstractConverter<EventInformation, Event>
        implements Converter<EventInformation, Event>, DomainConverter<Event> {

    private EventRepository eventRepository;
    private CheckpointRepository checkpointRepository;

    @Autowired
    public EventConverter(EventRepository eventRepository,
                          CheckpointRepository checkpointRepository) {
        this.eventRepository = eventRepository;
        this.checkpointRepository = checkpointRepository;
    }

    @Override
    protected Event map(EventInformation eventInformation) {
        Event event = get(eventInformation.getId());
        event.setName(eventInformation.getName());
        event.setStart(checkpointRepository.getOne(eventInformation.getStartId()));
        event.setFinish(checkpointRepository.getOne(eventInformation.getFinishId()));
        return event;
    }

    @Override
    public Event get(Long id) {
        if (id != null) {
            Optional<Event> event = eventRepository.findById(id);
            if (event.isPresent()) {
                return event.get();
            }
        }
        return new Event();
    }
}
