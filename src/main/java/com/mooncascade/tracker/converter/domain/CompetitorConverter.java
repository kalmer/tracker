package com.mooncascade.tracker.converter.domain;

import com.mooncascade.tracker.converter.AbstractConverter;
import com.mooncascade.tracker.converter.Converter;
import com.mooncascade.tracker.converter.DomainConverter;
import com.mooncascade.tracker.domain.Competitor;
import com.mooncascade.tracker.information.CompetitorInformation;
import com.mooncascade.tracker.repository.ChipRepository;
import com.mooncascade.tracker.repository.CompetitorRepository;
import com.mooncascade.tracker.repository.EventRepository;
import com.mooncascade.tracker.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CompetitorConverter extends AbstractConverter<CompetitorInformation, Competitor>
        implements Converter<CompetitorInformation, Competitor>, DomainConverter<Competitor> {

    private CompetitorRepository competitorRepository;
    private ChipRepository chipRepository;
    private PersonRepository personRepository;
    private EventRepository eventRepository;

    @Autowired
    public CompetitorConverter(CompetitorRepository competitorRepository,
                               ChipRepository chipRepository,
                               PersonRepository personRepository,
                               EventRepository eventRepository) {
        this.competitorRepository = competitorRepository;
        this.chipRepository = chipRepository;
        this.personRepository = personRepository;
        this.eventRepository = eventRepository;
    }

    @Override
    protected Competitor map(CompetitorInformation competitorInformation) {
        Competitor competitor = get(competitorInformation.getId());
        competitor.setNumber(competitorInformation.getNumber());
        competitor.setChip(chipRepository.getOne(competitorInformation.getChipId()));
        competitor.setPerson(personRepository.getOne(competitorInformation.getPersonId()));
        competitor.setEvent(eventRepository.getOne(competitorInformation.getEventId()));
        return competitor;
    }

    @Override
    public Competitor get(Long id) {
        if (id != null) {
            Optional<Competitor> competitor = competitorRepository.findById(id);
            if (competitor.isPresent()) {
                return competitor.get();
            }
        }
        return new Competitor();
    }
}
