package com.mooncascade.tracker.converter.information;

import com.mooncascade.tracker.converter.AbstractConverter;
import com.mooncascade.tracker.converter.Converter;
import com.mooncascade.tracker.domain.Result;
import com.mooncascade.tracker.information.ResultInformation;
import org.springframework.stereotype.Service;

@Service
public class ResultInformationConverter extends AbstractConverter<Result, ResultInformation>
        implements Converter<Result, ResultInformation> {

    @Override
    protected ResultInformation map(Result result) {
        ResultInformation information = new ResultInformation();
        information.setPerson(result.getCompetitor().getPerson().getName());
        information.setTime(result.getTime());
        return information;
    }
}
