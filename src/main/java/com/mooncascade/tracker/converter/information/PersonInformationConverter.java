package com.mooncascade.tracker.converter.information;

import com.mooncascade.tracker.converter.AbstractConverter;
import com.mooncascade.tracker.converter.Converter;
import com.mooncascade.tracker.domain.Person;
import com.mooncascade.tracker.information.PersonInformation;
import org.springframework.stereotype.Service;

@Service
public class PersonInformationConverter extends AbstractConverter<Person, PersonInformation>
        implements Converter<Person, PersonInformation> {


    @Override
    protected PersonInformation map(Person person) {
        PersonInformation information = new PersonInformation();
        information.setId(person.getId());
        information.setName(person.getName());
        return information;
    }
}
