package com.mooncascade.tracker.converter.information;

import com.mooncascade.tracker.converter.AbstractConverter;
import com.mooncascade.tracker.converter.Converter;
import com.mooncascade.tracker.domain.Event;
import com.mooncascade.tracker.information.EventInformation;
import org.springframework.stereotype.Service;

@Service
public class EventInformationConverter extends AbstractConverter<Event, EventInformation>
        implements Converter<Event, EventInformation> {

    @Override
    protected EventInformation map(Event event) {
        EventInformation information = new EventInformation();
        information.setId(event.getId());
        information.setName(event.getName());
        information.setStartId(event.getStart().getId());
        information.setFinishId(event.getFinish().getId());
        return information;
    }
}
