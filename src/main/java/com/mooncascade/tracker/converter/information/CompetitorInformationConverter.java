package com.mooncascade.tracker.converter.information;

import com.mooncascade.tracker.converter.AbstractConverter;
import com.mooncascade.tracker.converter.Converter;
import com.mooncascade.tracker.domain.Competitor;
import com.mooncascade.tracker.information.CompetitorInformation;
import org.springframework.stereotype.Service;

@Service
public class CompetitorInformationConverter extends AbstractConverter<Competitor, CompetitorInformation>
        implements Converter<Competitor, CompetitorInformation> {

    @Override
    protected CompetitorInformation map(Competitor competitor) {
        CompetitorInformation information = new CompetitorInformation();
        information.setId(competitor.getId());
        information.setNumber(competitor.getNumber());
        information.setEventId(competitor.getEvent().getId());
        information.setPersonId(competitor.getPerson().getId());
        information.setChipId(competitor.getChip().getId());
        return information;
    }
}
