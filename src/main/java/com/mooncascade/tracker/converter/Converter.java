package com.mooncascade.tracker.converter;

public interface Converter<SOURCE, TARGET> {

    TARGET convert(SOURCE source);

}
