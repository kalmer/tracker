package com.mooncascade.tracker.converter;

public interface DomainConverter<DOMAIN> {

    DOMAIN get(Long id);

}
