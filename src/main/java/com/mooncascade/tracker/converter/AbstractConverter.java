package com.mooncascade.tracker.converter;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractConverter<SOURCE, TARGET> implements Converter<SOURCE, TARGET> {

    @Override
    public TARGET convert(SOURCE source) {
        return source != null ? map(source) : null;
    }

    protected abstract TARGET map(SOURCE source);

    public List<TARGET> convertAll(List<SOURCE> sourceList) {
        if (sourceList != null && !sourceList.isEmpty()) {
            return sourceList.stream()
                    .map(this::convert)
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }
}