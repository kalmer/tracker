package com.mooncascade.tracker.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "start")
    private Checkpoint start;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "finish")
    private Checkpoint finish;

}
