package com.mooncascade.tracker.information;

import lombok.Data;

@Data
public class CompetitorInformation {

    private Long id;
    private Long number;
    private Long personId;
    private Long eventId;
    private Long chipId;

}
