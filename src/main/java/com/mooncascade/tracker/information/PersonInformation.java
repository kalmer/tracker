package com.mooncascade.tracker.information;

import lombok.Data;

@Data
public class PersonInformation {

    private Long id;
    private String name;

}
