package com.mooncascade.tracker.information;

import lombok.Data;

import java.util.Date;

@Data
public class ResultInformation {

    private Date time;
    private String person;

}
