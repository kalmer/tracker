package com.mooncascade.tracker.information;

import lombok.Data;

@Data
public class EventInformation {

    private Long id;
    private String name;
    private Long startId;
    private Long finishId;

}
