package com.mooncascade.tracker.controller;

import com.mooncascade.tracker.converter.domain.PersonConverter;
import com.mooncascade.tracker.converter.information.PersonInformationConverter;
import com.mooncascade.tracker.domain.Person;
import com.mooncascade.tracker.information.PersonInformation;
import com.mooncascade.tracker.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/persons")
public class PersonController {

    private PersonService personService;
    private PersonConverter personConverter;
    private PersonInformationConverter personInformationConverter;

    @Autowired
    public PersonController(PersonService personService,
                            PersonConverter personConverter,
                            PersonInformationConverter personInformationConverter) {
        this.personService = personService;
        this.personConverter = personConverter;
        this.personInformationConverter = personInformationConverter;
    }

    @PostMapping
    public PersonInformation save(@RequestBody PersonInformation personInformation) {
        Person person = personConverter.convert(personInformation);
        return personInformationConverter.convert(personService.save(person));
    }

    @PutMapping
    public PersonInformation update(@RequestBody PersonInformation personInformation) {
        Person person = personConverter.convert(personInformation);
        return personInformationConverter.convert(personService.save(person));
    }
}
