package com.mooncascade.tracker.controller;

import java.util.List;

import com.mooncascade.tracker.domain.Checkpoint;
import com.mooncascade.tracker.service.CheckpointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/checkpoints")
public class CheckpointController {

    private CheckpointService checkpointService;

    @Autowired
    public CheckpointController(CheckpointService checkpointService) {
        this.checkpointService = checkpointService;
    }

    @GetMapping
    public List<Checkpoint> list() {
        return checkpointService.list();
    }

    @PostMapping
    public Checkpoint create() {
        return checkpointService.create();
    }
}
