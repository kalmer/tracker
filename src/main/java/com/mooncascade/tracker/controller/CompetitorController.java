package com.mooncascade.tracker.controller;

import com.mooncascade.tracker.converter.domain.CompetitorConverter;
import com.mooncascade.tracker.converter.information.CompetitorInformationConverter;
import com.mooncascade.tracker.domain.Competitor;
import com.mooncascade.tracker.information.CompetitorInformation;
import com.mooncascade.tracker.service.CompetitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/competitors")
public class CompetitorController {

    private CompetitorService competitorService;
    private CompetitorConverter competitorConverter;
    private CompetitorInformationConverter competitorInformationConverter;

    @Autowired
    public CompetitorController(CompetitorService competitorService,
                                CompetitorConverter competitorConverter,
                                CompetitorInformationConverter competitorInformationConverter) {
        this.competitorService = competitorService;
        this.competitorConverter = competitorConverter;
        this.competitorInformationConverter = competitorInformationConverter;
    }

    @PostMapping
    public CompetitorInformation create(@RequestBody CompetitorInformation competitorInformation) {
        Competitor competitor = competitorConverter.convert(competitorInformation);
        return competitorInformationConverter.convert(competitorService.create(competitor));
    }
}
