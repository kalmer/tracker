package com.mooncascade.tracker.controller;

import java.util.List;

import com.mooncascade.tracker.domain.Chip;
import com.mooncascade.tracker.service.ChipService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/chips")
public class ChipController {

    private ChipService chipService;

    public ChipController(ChipService chipService) {
        this.chipService = chipService;
    }

    @GetMapping
    public List<Chip> list() {
        return chipService.list();
    }

    @PostMapping
    public Chip create() {
        return chipService.create();
    }
}
