package com.mooncascade.tracker.controller;

import com.mooncascade.tracker.converter.domain.EventConverter;
import com.mooncascade.tracker.converter.information.EventInformationConverter;
import com.mooncascade.tracker.converter.information.ResultInformationConverter;
import com.mooncascade.tracker.domain.Event;
import com.mooncascade.tracker.information.EventInformation;
import com.mooncascade.tracker.information.ResultInformation;
import com.mooncascade.tracker.service.Comparators;
import com.mooncascade.tracker.service.EventService;
import com.mooncascade.tracker.service.ResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/events")
public class EventController {

    private EventService eventService;
    private ResultService resultService;
    private EventConverter eventConverter;
    private EventInformationConverter eventInformationConverter;
    private ResultInformationConverter resultInformationConverter;

    @Autowired
    public EventController(EventService eventService,
                           ResultService resultService,
                           EventConverter eventConverter,
                           EventInformationConverter eventInformationConverter,
                           ResultInformationConverter resultInformationConverter) {
        this.eventService = eventService;
        this.resultService = resultService;
        this.eventConverter = eventConverter;
        this.eventInformationConverter = eventInformationConverter;
        this.resultInformationConverter = resultInformationConverter;
    }

    @GetMapping("/{eventId}")
    public EventInformation get(@PathVariable("eventId") Long eventId) {
        return eventInformationConverter.convert(eventService.get(eventId));
    }

    @GetMapping
    public List<EventInformation> list() {
        return eventInformationConverter.convertAll(eventService.list());
    }

    @PostMapping
    public EventInformation create(@RequestBody EventInformation eventInformation) {
        Event event = eventConverter.convert(eventInformation);
        return eventInformationConverter.convert(eventService.create(event));
    }

    @PostMapping("/{eventId}/start/{chipId}")
    public List<ResultInformation> start(@PathVariable("eventId") Long eventId,
                                         @PathVariable("chipId") Long chipId) {
        List<ResultInformation> results = resultInformationConverter.convertAll(resultService.start(eventId, chipId, new Date()));
        results.sort(Comparators.RESULT_COMPARATOR);
        return results;
    }

    @PostMapping("/{eventId}/finish/{chipId}")
    public List<ResultInformation> end(@PathVariable("eventId") Long eventId,
                                       @PathVariable("chipId") Long chipId) {
        List<ResultInformation> results = resultInformationConverter.convertAll(resultService.finish(eventId, chipId, new Date()));
        results.sort(Comparators.RESULT_COMPARATOR);
        return results;
    }
}
