package com.mooncascade.tracker.repository;

import com.mooncascade.tracker.domain.Competitor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompetitorRepository extends JpaRepository<Competitor, Long> {

    @Query("SELECT c FROM Competitor c JOIN c.event e JOIN c.chip ch WHERE e.id = :eventId AND ch.id = :chipId")
    List<Competitor> registeredToEvent(@Param("eventId") Long eventId, @Param("chipId") Long chipId);

}
