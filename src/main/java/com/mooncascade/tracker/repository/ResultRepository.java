package com.mooncascade.tracker.repository;

import com.mooncascade.tracker.domain.Result;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ResultRepository extends JpaRepository<Result, Long> {

    @Query("SELECT r FROM Result r JOIN r.event e JOIN r.checkpoint c WHERE e.id = :eventId AND c.id = :checkpointId")
    List<Result> findBy(@Param("eventId") Long eventId, @Param("checkpointId") Long checkpointId);

}
