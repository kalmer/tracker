package com.mooncascade.tracker.repository;

import com.mooncascade.tracker.domain.Chip;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChipRepository extends JpaRepository<Chip, Long> {
}
