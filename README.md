# SPORDIVÕISTLUSE AJAVÕTUSÜSTEEM (BACKEND)

Spordivõistluse ajavõtusüsteem on java backendi infosüsteem, mis kasutab Postgres andmebaasi.

## Rakenduse käivitamine
Rakenduse käivitamiseks on vajalik Postgres andmebaas, andmebaasi konfiguratsioon asub pom.xml failis:

```xml
<database.user>postgres</database.user>
<database.password>postgres</database.password>
<database.url>jdbc:postgresql://localhost:5432/postgres</database.url>
```

Rakenust saab käivitada kahel erineval viisil:
1. Kasutades IntelliJ IDE-t improdi maveni projektina ja käivita TrackerApplication.java (Sellisel juhul peab Run configurations eelnevalt ka rakenduse ehitama)
2. Komplieeri rakendus kasutades mavenit (https://maven.apache.org/install.html), käsureal käivita projekti root folderis 'mvn install' ning navigeeri '/target' kataloogi, et 'java -jar tracker.jar'

## Rakenduse testimine

Edukal käivitamisel saab rakenduse poole pöörduda: localhost:8080

### REST endpointid
**/checkpoints** POST - loob uue checkpointi ning tagastab checkpoint-i id, mida saab kasutada spordiürituse ajavõtu punktina (Finishikordidori sisenemisel või ületamisel)

**/checkpoints** GET - tagastab kõik olemasolevad checkpointid

**/chips** POST - loob uue ajavõtu kiibi, mida saab kinnitada sportlase külge

**/chips** GET - tagastab kõik kiibid

**/persons** POST - võimaldab luua sportlase, sisendiks:
```javascript
{
    "name" : "Eesnimi Perenimi"
}
```

**/persons** PUT - võimaldab muuta olemasoleva sportlase andmed, sisendiks:
```javascript
{
    "id" : 1
    "name" : "Uus-Eesnimi Perenimi"
}
```

**/events** GET - tagastab listi kõikidest spordivõistlustest

**/events/{eventId}** GET - tagastab sprodivõistluse andmed vastavalt id-le

**/events** POST - võimaldab luua spordivõistluse, mis kasutab eelnevalt loodud checkpointe, sisendiks:
```javascript
{
	"name" : "Tallinna maraton",
	"startId" : 1,
	"finishId" : 2 
}
```
Checkpointe saab määrata startdId ja finishId väärtusteks, mis on vastavalt finishikoridori sisenemine ja ületamine. Vastavad checkpointid peavad eksisteerima checkpoint tabelis eelnevalt

**/competitors** POST - Võimaldab registreerida sportlasi vastava numbriga võistlusele, sisendiks:
```javascript
{
	"number" : 101,
	"personId" : 1,
	"eventId" : 1,
	"chipId" : 1
}
```
Antud sisendil on number sportlase võistlusnumber, personId viitab loodud sportlasele, eventId viitab võistlusele ja chipId viitab kiibile, mida sportlane antud võistlusel kasutab. Kõik andmed peavad eelnevalt eksitsteerima

**/events/{eventId}/start/{chipId}** POST - Antud endpointile saadab checkpoint andmed võistluse kohta (eventId), kus seda kasutatakse, ja kiibi kohta (chipId), mille andmed loeti. Finishikoridori sisenemisel loetud kiibi id saadetakse vastava võistluse alla, eventId peab eksisteerima ning vastava kiibiga sportlane peab olema registreerinud võistlusele (comptetitors). Tagastab vastava start checkpointi tulemuste listi

**/events/{eventId}/finish/{chipId}** POST - Antud endpointile saadab checkpoint andmed võistluse kohta (eventId), kus seda kasutatakse, ja kiibi kohta (chipId), mille andmed loeti. Finishikoridori ületamisel loetud kiibi id saadetakse vastava võistluse alla, eventId peab eksisteerima ning vastava kiibiga sportlane peab olema registreerinud võistlusele (comptetitors). Tagastab vastava finish checkpointi tulemuste listi


